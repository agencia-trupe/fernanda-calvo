@extends('frontend.common.template')

@section('content')

    <div class="main depoimentos">
        <h1>DEPOIMENTOS</h1>

        @foreach($depoimentos as $depoimento)
        <div class="depoimento">
            {!! $depoimento->texto !!}
        </div>
        @endforeach
    </div>

@endsection
