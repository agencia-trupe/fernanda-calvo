@extends('frontend.common.template')

@section('content')

    <div class="main clipping">
        <h1>CLIPPING</h1>

        <div class="thumbs">
            @foreach($clippings as $clipping)
                @if($clipping->video_tipo && $clipping->video_codigo)
                    @if($clipping->capa)
                        <a href="{{ $clipping->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$clipping->video_codigo : 'https://player.vimeo.com/video/'.$clipping->video_codigo }}" class="clipping-video capa">
                            <img src="{{ asset('assets/img/clipping/'.$clipping->capa) }}" alt="">
                            <div class="overlay">
                                <span>{{ $clipping->titulo }}</span>
                            </div>
                        </a>
                    @else
                        <a href="{{ $clipping->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$clipping->video_codigo : 'https://player.vimeo.com/video/'.$clipping->video_codigo }}" class="clipping-video">
                            <div class="titulo">
                                <span>{{ $clipping->titulo }}</span>
                            </div>
                        </a>
                    @endif

                @elseif($clipping->link)
                    @if($clipping->capa)
                        <a href="{{ $clipping->link }}" target="_blank" class="clipping-link capa">
                            <img src="{{ asset('assets/img/clipping/'.$clipping->capa) }}" alt="">
                            <div class="overlay">
                                <span>{{ $clipping->titulo }}</span>
                            </div>
                        </a>
                    @else
                        <a href="{{ $clipping->link }}" target="_blank" class="clipping-link">
                            <div class="titulo">
                                <span>{{ $clipping->titulo }}</span>
                            </div>
                        </a>
                    @endif

                @else
                    @if($clipping->capa)
                        <a href="#" data-galeria="{{ $clipping->id }}" class="clipping-galeria capa">
                            <img src="{{ asset('assets/img/clipping/'.$clipping->capa) }}" alt="">
                            <div class="overlay">
                                <span>{{ $clipping->titulo }}</span>
                            </div>
                        </a>
                    @else
                        <a href="#" data-galeria="{{ $clipping->id }}" class="clipping-galeria">
                            <div class="titulo">
                                <span>{{ $clipping->titulo }}</span>
                            </div>
                        </a>
                    @endif
                @endif
            @endforeach
        </div>
    </div>

    <div class="hidden">
    @foreach($clippings as $c)
        @foreach($c->imagens as $imagem)
            <a href="{{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria{{ $c->id }}"></a>
        @endforeach
    @endforeach
    </div>

@endsection
