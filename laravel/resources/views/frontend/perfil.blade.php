@extends('frontend.common.template')

@section('content')

    <div class="main perfil">
        <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}" alt="">
        <div class="texto">
            <h1>PERFIL</h1>
            {!! $perfil->texto !!}
        </div>
    </div>

@endsection
