@extends('frontend.common.template')

@section('content')

    <div class="main projetos">
        <div class="categorias">
            <h1>PROJETOS</h1>
            @foreach($categorias as $c)
            <a href="{{ route('projetos', $c->slug) }}" @if($c->slug == $categoria->slug) class="active" @endif>{{ $c->titulo }}</a>
            @endforeach
        </div>

        <div class="projetos-show">
            <div class="descricao">
                <h2>{{ $projeto->titulo }}</h2>
                {!! $projeto->descricao !!}
            </div>

            <div class="imagens">
                @foreach($projeto->imagens as $imagem)
                <a href="{{ asset('assets/img/projetos/imagens/ampliacao/'.$imagem->imagem) }}" rel="galeria">
                    <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" alt="">
                </a>
                @endforeach
                <div class="cycle-prev" style="z-index:102"></div>
                <div class="cycle-next" style="z-index:102"></div>
            </div>
        </div>
    </div>

@endsection
