@extends('frontend.common.template')

@section('content')

    <div class="main projetos">
        <div class="categorias">
            <h1>PROJETOS</h1>
            @foreach($categorias as $c)
            <a href="{{ route('projetos', $c->slug) }}" @if($c->slug == $categoria->slug) class="active" @endif>{{ $c->titulo }}</a>
            @endforeach
        </div>

        <div class="projetos-thumbs">
            @foreach($projetos as $projeto)
            <a href="{{ route('projetos.show', [$projeto->categoria->slug, $projeto->slug]) }}">
                <div class="imagem">
                    <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                </div>
                <span>{{ $projeto->titulo }}</span>
            </a>
            @endforeach
        </div>
    </div>

@endsection
