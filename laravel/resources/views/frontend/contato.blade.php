@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <h1>CONTATO</h1>

        <div class="col">
            <h2>{{ $contato->telefone }}</h2>

            <form action="" id="form-contato" method="POST">
                <h3>Fale Conosco</h3>
                <input type="text" name="nome" id="nome" placeholder="NOME" required>
                <input type="text" name="telefone" id="telefone" placeholder="TELEFONE">
                <input type="email" name="email" id="email" placeholder="E-MAIL" required>
                <textarea name="mensagem" id="mensagem" placeholder="MENSAGEM" required></textarea>
                <div id="form-contato-response"></div>
                <input type="submit" value="ENVIAR">
            </form>
        </div>

        <div class="col">
            <div class="endereco">{!! $contato->endereco !!}</div>

            <div class="mapa">{!! $contato->google_maps !!}</div>
        </div>
    </div>

@endsection
