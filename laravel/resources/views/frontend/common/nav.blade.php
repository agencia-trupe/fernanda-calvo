<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>Home</a>
<a href="{{ route('perfil') }}" @if(Tools::isActive('perfil')) class="active" @endif>Perfil</a>
<a href="{{ route('projetos') }}" @if(Tools::isActive('projetos*')) class="active" @endif>Projetos</a>
<a href="{{ route('depoimentos') }}" @if(Tools::isActive('depoimentos')) class="active" @endif>Depoimentos</a>
<a href="{{ route('clipping') }}" @if(Tools::isActive('clipping')) class="active" @endif>Clipping</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>Contato</a>

<div class="social">
    @foreach(['facebook', 'instagram'] as $s)
    @if($contato->{$s})
    <a href="{{ $contato->{$s} }}" class="{{ $s }}">{{ $s }}</a>
    @endif
    @endforeach
</div>
