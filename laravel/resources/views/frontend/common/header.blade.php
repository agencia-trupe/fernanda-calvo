    <header>
        <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
        <nav>
            @include('frontend.common.nav')
        </nav>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>
