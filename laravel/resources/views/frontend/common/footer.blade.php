    <footer>
        <div class="center">
            <p class="info">
                <span><strong>FERNANDA CALVO &middot; DESIGN DE INTERIORES</strong></span>
                <span><strong>{{ $contato->telefone }}</strong></span>
                <span>{!! str_replace(['<br>', '<br />', '<br/>'], ' - ', $contato->endereco) !!}</span>
            </p>
            <p class="copyright">
                © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
