import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Banners from './Banners';
import GaleriaProjetos from './GaleriaProjetos';
import FormularioContato from './FormularioContato';

AjaxSetup();
MobileToggle();
Banners();
GaleriaProjetos();
FormularioContato();
