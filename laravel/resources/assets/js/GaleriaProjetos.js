export default function GaleriaProjetos() {
    $('.projetos-show .imagens').cycle({
        slides: '>a',
        allowWrap: false,
        timeout: 0
    });

    $('.projetos-show .imagens a').fancybox({
        padding: 10,
        loop: false
    });

    $('.clipping-video').fancybox({
        padding: 0,
        type: 'iframe',
        width: 800,
        height: 450,
        aspectRatio: true
    });

    $('.fancybox').fancybox({
        padding: 10,
        loop: false
    });

    $('.clipping-galeria').click(function(e) {
        var el, id = $(this).data('galeria');
        if(id){
            el = $('.fancybox[rel=galeria' + id + ']:eq(0)');
            e.preventDefault();
            el.click();
        }
    });
};
