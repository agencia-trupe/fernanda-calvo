<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosTable extends Migration
{
    public function up()
    {
        Schema::create('projetos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('projetos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('projetos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('capa');
            $table->string('titulo');
            $table->text('descricao');
            $table->foreign('projetos_categoria_id')->references('id')->on('projetos_categorias')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('projetos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('projeto_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('projeto_id')->references('id')->on('projetos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('projetos_imagens');
        Schema::drop('projetos');
        Schema::drop('projetos_categorias');
    }
}
