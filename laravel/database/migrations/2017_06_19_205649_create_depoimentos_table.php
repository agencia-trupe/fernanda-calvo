<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepoimentosTable extends Migration
{
    public function up()
    {
        Schema::create('depoimentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('depoimentos');
    }
}
