<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PerfilRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'image',
            'texto' => 'required',
        ];
    }
}
