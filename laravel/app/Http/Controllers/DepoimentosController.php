<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Depoimento;

class DepoimentosController extends Controller
{
    public function index()
    {
        $depoimentos = Depoimento::ordenados()->get();

        return view('frontend.depoimentos', compact('depoimentos'));
    }
}
