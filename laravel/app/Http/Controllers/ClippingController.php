<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Clipping;

class ClippingController extends Controller
{
    public function index()
    {
        $clippings = Clipping::ordenados()->get();

        return view('frontend.clipping', compact('clippings'));
    }
}
