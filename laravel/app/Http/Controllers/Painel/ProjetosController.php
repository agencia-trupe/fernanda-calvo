<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Models\ProjetoCategoria;

class ProjetosController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = ProjetoCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (ProjetoCategoria::find($filtro)) {
            $registros = Projeto::where('projetos_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = Projeto::leftJoin('projetos_categorias as cat', 'cat.id', '=', 'projetos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('projetos.*')
                ->ordenados()->get();
        }

        return view('painel.projetos.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.projetos.create', compact('categorias'));
    }

    public function store(ProjetosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Projeto::upload_capa();

            Projeto::create($input);

            return redirect()->route('painel.projetos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Projeto $registro)
    {
        $categorias = $this->categorias;

        return view('painel.projetos.edit', compact('registro', 'categorias'));
    }

    public function update(ProjetosRequest $request, Projeto $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Projeto::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.projetos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Projeto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.projetos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
