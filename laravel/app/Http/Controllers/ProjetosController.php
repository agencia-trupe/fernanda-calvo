<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ProjetoCategoria;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index(ProjetoCategoria $categoria)
    {
        $categorias = ProjetoCategoria::ordenados()->get();

        if (!$categoria->exists) {
            $categoria = $categorias->first();
        }

        $projetos = $categoria->projetos()->get();

        return view('frontend.projetos.index', compact('categorias', 'categoria', 'projetos'));
    }

    public function show(ProjetoCategoria $categoria, Projeto $projeto)
    {
        $categorias = ProjetoCategoria::ordenados()->get();

        return view('frontend.projetos.show', compact('categorias', 'categoria', 'projeto'));
    }
}
