<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('projetos/{projetos_categoria?}', 'ProjetosController@index')->name('projetos');
    Route::get('projetos/{projetos_categoria}/{projeto_slug}', 'ProjetosController@show')->name('projetos.show');
    Route::get('clipping', 'ClippingController@index')->name('clipping');
    Route::get('depoimentos', 'DepoimentosController@index')->name('depoimentos');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::get('clipping/delete-capa/{clipping}', 'ClippingController@deleteCapa')->name('painel.clipping.delete-capa');
		Route::resource('clipping', 'ClippingController');
		Route::get('clipping/{clipping}/imagens/clear', [
			'as'   => 'painel.clipping.imagens.clear',
			'uses' => 'ClippingImagensController@clear'
		]);
		Route::resource('clipping.imagens', 'ClippingImagensController', ['parameters' => ['imagens' => 'imagens_clipping']]);
		Route::resource('projetos/categorias', 'ProjetosCategoriasController', ['parameters' => ['categorias' => 'categorias_projetos']]);
		Route::resource('projetos', 'ProjetosController');
		Route::get('projetos/{projetos}/imagens/clear', [
			'as'   => 'painel.projetos.imagens.clear',
			'uses' => 'ProjetosImagensController@clear'
		]);
		Route::resource('projetos.imagens', 'ProjetosImagensController', ['parameters' => ['imagens' => 'imagens_projetos']]);
		Route::resource('depoimentos', 'DepoimentosController');
		Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
