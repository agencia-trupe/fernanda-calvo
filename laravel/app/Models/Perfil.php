<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Perfil extends Model
{
    protected $table = 'perfil';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 445,
            'height' => null,
            'path'   => 'assets/img/perfil/'
        ]);
    }

}
