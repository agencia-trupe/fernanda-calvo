-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: fernandacalvo
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,0,'pexels-photo-24578_20170622035614.jpg','2017-06-22 03:56:15','2017-06-22 03:56:15'),(2,0,'pexels-photo-69040_20170622035621.png','2017-06-22 03:56:22','2017-06-22 03:56:22');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clipping`
--

DROP TABLE IF EXISTS `clipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clipping`
--

LOCK TABLES `clipping` WRITE;
/*!40000 ALTER TABLE `clipping` DISABLE KEYS */;
INSERT INTO `clipping` VALUES (1,0,'Trupe','','http://www.trupe.net','','','2017-06-22 04:00:35','2017-06-22 04:00:35'),(2,0,'Exemplo','','','','','2017-06-22 04:00:41','2017-06-22 04:00:41');
/*!40000 ALTER TABLE `clipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clipping_imagens`
--

DROP TABLE IF EXISTS `clipping_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clipping_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clipping_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clipping_imagens_clipping_id_foreign` (`clipping_id`),
  CONSTRAINT `clipping_imagens_clipping_id_foreign` FOREIGN KEY (`clipping_id`) REFERENCES `clipping` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clipping_imagens`
--

LOCK TABLES `clipping_imagens` WRITE;
/*!40000 ALTER TABLE `clipping_imagens` DISABLE KEYS */;
INSERT INTO `clipping_imagens` VALUES (1,2,0,'pexels-photo-38132_20170622040051.jpeg','2017-06-22 04:00:51','2017-06-22 04:00:51'),(2,2,0,'blue-abstract-glass-balls_20170622040057.jpg','2017-06-22 04:00:58','2017-06-22 04:00:58'),(3,2,0,'pattern-abstract-honeycomb-metal_20170622040057.jpg','2017-06-22 04:00:58','2017-06-22 04:00:58');
/*!40000 ALTER TABLE `clipping_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@trupe.net','+55 11 99810 7728','Avenida Andr&ocirc;meda, 885, cj. 507<br />\r\nEdif&iacute;cio Brascan Century Plaza<br />\r\nAlphaville / Barueri - SP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3659.314266390943!2d-46.86694888451725!3d-23.485187384720774!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf03b2b0aa6895%3A0xf183de33f0826ae7!2sAv.+Andr%C3%B4meda%2C+885+-+Alphaville+Empresarial%2C+Barueri+-+SP!5e0!3m2!1spt-BR!2sbr!4v1498103971689\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>','#','#',NULL,'2017-06-22 03:59:37');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `depoimentos`
--

DROP TABLE IF EXISTS `depoimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `depoimentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `depoimentos`
--

LOCK TABLES `depoimentos` WRITE;
/*!40000 ALTER TABLE `depoimentos` DISABLE KEYS */;
INSERT INTO `depoimentos` VALUES (1,3,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in lectus vel velit lobortis elementum in ac ex. Cras tristique velit vel enim blandit feugiat. Vivamus sed ante eget nunc posuere mattis. Suspendisse diam quam, eleifend gravida turpis quis, cursus molestie lacus.</p>\r\n\r\n<p>Aliquam egestas nec massa sed feugiat. Sed vel metus venenatis, ullamcorper ante eget, congue metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec id rhoncus nunc. Ut nulla mi, scelerisque eu suscipit sit amet, mattis vitae ligula. In quis neque placerat, luctus metus at, eleifend leo. Vestibulum placerat euismod mauris, semper mollis dolor rutrum a.</p>\r\n','2017-06-22 03:58:00','2017-06-22 03:58:00'),(2,1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in lectus vel velit lobortis elementum in ac ex. Cras tristique velit vel enim blandit feugiat. Vivamus sed ante eget nunc posuere mattis. Suspendisse diam quam, eleifend gravida turpis quis, cursus molestie lacus.</p>\r\n\r\n<p>Aliquam egestas nec massa sed feugiat. Sed vel metus venenatis, ullamcorper ante eget, congue metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec id rhoncus nunc. Ut nulla mi, scelerisque eu suscipit sit amet, mattis vitae ligula. In quis neque placerat, luctus metus at, eleifend leo. Vestibulum placerat euismod mauris, semper mollis dolor rutrum a.</p>\r\n','2017-06-22 03:58:14','2017-06-22 03:58:14'),(3,2,'<p>Vivamus malesuada nunc lorem, vitae tincidunt orci convallis a. In tincidunt aliquam tortor vitae euismod. Aenean nec felis at quam volutpat mattis ac quis eros.</p>\r\n\r\n<p>Nullam gravida diam eget fermentum tincidunt. Maecenas pellentesque varius iaculis. Duis finibus quis tellus eu euismod. Integer suscipit urna ut nisi consectetur, vitae faucibus justo dapibus.&nbsp;</p>\r\n','2017-06-22 03:58:22','2017-06-22 03:58:22');
/*!40000 ALTER TABLE `depoimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2017_06_19_205035_create_banners_table',1),('2017_06_19_205159_create_perfil_table',1),('2017_06_19_205649_create_depoimentos_table',1),('2017_06_19_205927_create_projetos_table',1),('2017_06_19_212021_create_clipping_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES (1,'foto-perfil_20170622035718.jpg','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in lectus vel velit lobortis elementum in ac ex. Cras tristique velit vel enim blandit feugiat. Vivamus sed ante eget nunc posuere mattis. Suspendisse diam quam, eleifend gravida turpis quis, cursus molestie lacus. Aliquam egestas nec massa sed feugiat.</p>\r\n\r\n<p>Sed vel metus venenatis, ullamcorper ante eget, congue metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec id rhoncus nunc. Ut nulla mi, scelerisque eu suscipit sit amet, mattis vitae ligula. In quis neque placerat, luctus metus at, eleifend leo. Vestibulum placerat euismod mauris, semper mollis dolor rutrum a.</p>\r\n\r\n<p>Vivamus malesuada nunc lorem, vitae tincidunt orci convallis a. In tincidunt aliquam tortor vitae euismod. Aenean nec felis at quam volutpat mattis ac quis eros.</p>\r\n\r\n<p>Nullam gravida diam eget fermentum tincidunt. Maecenas pellentesque varius iaculis. Duis finibus quis tellus eu euismod. Integer suscipit urna ut nisi consectetur, vitae faucibus justo dapibus.</p>\r\n',NULL,'2017-06-22 03:57:18');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos`
--

DROP TABLE IF EXISTS `projetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projetos_categoria_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_projetos_categoria_id_foreign` (`projetos_categoria_id`),
  CONSTRAINT `projetos_projetos_categoria_id_foreign` FOREIGN KEY (`projetos_categoria_id`) REFERENCES `projetos_categorias` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos`
--

LOCK TABLES `projetos` WRITE;
/*!40000 ALTER TABLE `projetos` DISABLE KEYS */;
INSERT INTO `projetos` VALUES (1,3,0,'exemplo','pexels-photo-1_20170622040159.jpg','Exemplo','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in lectus vel velit lobortis elementum in ac ex. Cras tristique velit vel enim blandit feugiat.</p>\r\n','2017-06-22 04:01:59','2017-06-22 04:01:59');
/*!40000 ALTER TABLE `projetos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_categorias`
--

DROP TABLE IF EXISTS `projetos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_categorias`
--

LOCK TABLES `projetos_categorias` WRITE;
/*!40000 ALTER TABLE `projetos_categorias` DISABLE KEYS */;
INSERT INTO `projetos_categorias` VALUES (1,0,'mostras','mostras','2017-06-22 04:01:34','2017-06-22 04:01:34'),(2,0,'corporativos','corporativos','2017-06-22 04:01:37','2017-06-22 04:01:37'),(3,0,'residenciais','residenciais','2017-06-22 04:01:40','2017-06-22 04:01:40');
/*!40000 ALTER TABLE `projetos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_imagens`
--

DROP TABLE IF EXISTS `projetos_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`),
  CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens`
--

LOCK TABLES `projetos_imagens` WRITE;
/*!40000 ALTER TABLE `projetos_imagens` DISABLE KEYS */;
INSERT INTO `projetos_imagens` VALUES (1,1,0,'blue-abstract-glass-balls_20170622040205.png','2017-06-22 04:02:07','2017-06-22 04:02:07'),(2,1,0,'pattern-abstract-honeycomb-metal_20170622040215.png','2017-06-22 04:02:19','2017-06-22 04:02:19'),(3,1,0,'architect-graphics-abstract-building_20170622040215.png','2017-06-22 04:02:19','2017-06-22 04:02:19');
/*!40000 ALTER TABLE `projetos_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$RYC5woUTx8VWKf08032aOujE/ICGa7F3Ae2Ue91lxfnH2drUY2dJy','dARH0Wl2IcHNu3M6kmQdtJgasE0e0FptD2nbBC6eIHVGDW3rchIjofwGpURg',NULL,'2017-06-22 03:56:04');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-22  4:03:18
